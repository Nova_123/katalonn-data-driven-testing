<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Remove_1_2_3_4_5</name>
   <tag></tag>
   <elementGuidId>7d78267c-caac-4fc0-964a-e2ab37f4c5c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='remove-test.allthethings()-t-shirt-(red)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>262ed0ff-a3ff-464a-bcc4-71e29995b747</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn_secondary btn_small btn_inventory</value>
      <webElementGuid>f85f3487-ce21-416e-b00e-38cfb23f26fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>remove-test.allthethings()-t-shirt-(red)</value>
      <webElementGuid>efb9e034-c0c6-43e1-9b08-ea53cca81f52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>remove-test.allthethings()-t-shirt-(red)</value>
      <webElementGuid>b9e64996-9931-4582-b1ac-ee5e624361bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>remove-test.allthethings()-t-shirt-(red)</value>
      <webElementGuid>1e29c874-60a0-4481-8bcf-7e889fc2eab6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Remove</value>
      <webElementGuid>87b8bd69-87d2-4c23-8c4b-83821ad0196f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;remove-test.allthethings()-t-shirt-(red)&quot;)</value>
      <webElementGuid>bfbd0b3b-1eb5-412b-8d42-851684bcc4de</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='remove-test.allthethings()-t-shirt-(red)']</value>
      <webElementGuid>81f9a150-159d-431d-8dba-fb2cb1125997</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inventory_container']/div/div[6]/div[2]/div[2]/button</value>
      <webElementGuid>7174f715-99cb-45c0-b7d7-4bbed8984a28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$15.99'])[2]/following::button[1]</value>
      <webElementGuid>6b6e5684-8cd8-428f-a5e0-b87a20babea9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test.allTheThings() T-Shirt (Red)'])[1]/following::button[1]</value>
      <webElementGuid>837390e6-ef45-4214-be14-b5b72c42b107</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Twitter'])[1]/preceding::button[1]</value>
      <webElementGuid>7726948a-9007-4261-b0b4-c29a002239f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Facebook'])[1]/preceding::button[1]</value>
      <webElementGuid>ba644d1a-9c51-40b2-b85c-da3ced2f52ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Remove']/parent::*</value>
      <webElementGuid>1fcceed9-c6f1-4236-b3a1-3827c4cac26a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div[2]/div[2]/button</value>
      <webElementGuid>8ec6de04-2d11-4ba7-ac81-d3eabf962e4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'remove-test.allthethings()-t-shirt-(red)' and @name = 'remove-test.allthethings()-t-shirt-(red)' and (text() = 'Remove' or . = 'Remove')]</value>
      <webElementGuid>49e5f931-b74b-4d29-a293-46bd0462f8ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
