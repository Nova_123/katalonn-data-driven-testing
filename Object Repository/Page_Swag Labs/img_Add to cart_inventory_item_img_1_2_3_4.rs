<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Add to cart_inventory_item_img_1_2_3_4</name>
   <tag></tag>
   <elementGuidId>ec252031-f894-4285-8e88-b9e9efa7418a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#item_3_img_link > img.inventory_item_img</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='item_3_img_link']/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>629a680f-3c4b-4cdf-9572-b09c0512c977</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Test.allTheThings() T-Shirt (Red)</value>
      <webElementGuid>7c7c2278-7b0a-44ee-b099-7e0c5de14db0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>inventory_item_img</value>
      <webElementGuid>182c8544-8604-48e4-a1a8-a07e42efcecd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/static/media/red-tatt-1200x1500.30dadef4.jpg</value>
      <webElementGuid>71713662-5fd5-4704-b800-6c3aba7dd2d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;item_3_img_link&quot;)/img[@class=&quot;inventory_item_img&quot;]</value>
      <webElementGuid>363dc83a-eff3-4cfc-a492-449eec536ac4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='item_3_img_link']/img</value>
      <webElementGuid>9971e716-4bf3-44e3-8495-1d3b4028d974</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Test.allTheThings() T-Shirt (Red)']</value>
      <webElementGuid>885f2458-94e7-4116-822f-0641072ce5ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/a/img</value>
      <webElementGuid>dcf6cd40-6e0f-466b-ab3f-cc496dc8b552</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@alt = 'Test.allTheThings() T-Shirt (Red)' and @src = '/static/media/red-tatt-1200x1500.30dadef4.jpg']</value>
      <webElementGuid>118eac1a-607c-450d-8165-c55b3c0dc99f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
