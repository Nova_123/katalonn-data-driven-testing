<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add to cart</name>
   <tag></tag>
   <elementGuidId>2ac88529-e30e-4d1d-aa8b-32c7ff7bc857</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#add-to-cart-sauce-labs-backpack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='add-to-cart-sauce-labs-backpack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>fe7d6ae5-fcc4-42d0-ba8e-8b48ac47ff52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn_primary btn_small btn_inventory</value>
      <webElementGuid>8e318318-f81e-4c33-a678-419ad0503c79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-backpack</value>
      <webElementGuid>93ec783f-1f94-40ec-8be9-09fe5c1196f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-backpack</value>
      <webElementGuid>06e63e28-dc3f-4aa2-95cd-2e1f3fc07ebe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-backpack</value>
      <webElementGuid>ce3bcdcd-0b95-4825-a111-e0207da15d74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>ef71f52b-171b-4eef-aaf9-0b9611ff6bcf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-to-cart-sauce-labs-backpack&quot;)</value>
      <webElementGuid>163265c0-7cfb-4af8-8622-713d0951c08c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='add-to-cart-sauce-labs-backpack']</value>
      <webElementGuid>a6cf1adf-73eb-462f-af71-59c4adfa36fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inventory_container']/div/div/div[2]/div[2]/button</value>
      <webElementGuid>ed980948-f165-4f05-bc1b-0a34bc407f55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$29.99'])[1]/following::button[1]</value>
      <webElementGuid>fcc5e125-017d-4547-89a1-8a113cd47a3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Backpack'])[1]/following::button[1]</value>
      <webElementGuid>c841fcd2-e1e3-4ee0-b5f2-892bef5c0395</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Bike Light'])[1]/preceding::button[1]</value>
      <webElementGuid>d6aafea2-d9cd-42eb-9900-63b7e31e956a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$9.99'])[1]/preceding::button[1]</value>
      <webElementGuid>6eb83f72-960a-452f-a3ff-fc37374a1b62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add to cart']/parent::*</value>
      <webElementGuid>dd265bb4-e44f-4bf6-88e9-433f1e350515</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>f5e61fec-6d84-4619-8843-4518bcb31fc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'add-to-cart-sauce-labs-backpack' and @name = 'add-to-cart-sauce-labs-backpack' and (text() = 'Add to cart' or . = 'Add to cart')]</value>
      <webElementGuid>1df44f03-ee4d-439d-a94c-404a22920054</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
