<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Remove_1_2_3_4</name>
   <tag></tag>
   <elementGuidId>2ba86292-f38c-46f7-897e-8cc614f27fb3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#remove-sauce-labs-onesie</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='remove-sauce-labs-onesie']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>459bf927-c96f-45e4-95a5-b08e7c48f96a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn_secondary btn_small btn_inventory</value>
      <webElementGuid>18b525e0-9d9d-4ed8-b2ba-c6bb90fc9c06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>remove-sauce-labs-onesie</value>
      <webElementGuid>e57fba8a-fd3c-40ea-b76c-9756c0d2b827</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>remove-sauce-labs-onesie</value>
      <webElementGuid>bb0e3ea7-97dd-4b96-bd08-b6a6072e1c38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>remove-sauce-labs-onesie</value>
      <webElementGuid>4f7e0bf3-48b2-461d-8582-9b2a58cf8fa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Remove</value>
      <webElementGuid>d6acc329-69fb-40fd-bb9c-0958d0306905</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;remove-sauce-labs-onesie&quot;)</value>
      <webElementGuid>93197f0e-7397-4d79-9605-9de6be01ea92</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='remove-sauce-labs-onesie']</value>
      <webElementGuid>394fd45c-dcee-4e80-9d7b-5816fc74ef6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inventory_container']/div/div[5]/div[2]/div[2]/button</value>
      <webElementGuid>791659b9-76db-434c-96f9-58b68afcb420</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$7.99'])[1]/following::button[1]</value>
      <webElementGuid>9eacef15-b720-423b-b93f-b90de2670518</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Onesie'])[1]/following::button[1]</value>
      <webElementGuid>01e60f9a-0b64-4489-a12b-9fc3a1438b24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test.allTheThings() T-Shirt (Red)'])[1]/preceding::button[1]</value>
      <webElementGuid>1af5b3b6-b91e-4ee1-b9a8-70ec0b103935</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$15.99'])[2]/preceding::button[1]</value>
      <webElementGuid>d5d3c54f-bca5-46f5-8b39-25d399e54c2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Remove']/parent::*</value>
      <webElementGuid>1ce967cd-5368-43e6-8eee-4eec07ee02b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/div[2]/button</value>
      <webElementGuid>b476a965-56b4-43fb-bd80-d36d95238ef2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'remove-sauce-labs-onesie' and @name = 'remove-sauce-labs-onesie' and (text() = 'Remove' or . = 'Remove')]</value>
      <webElementGuid>a61a78ee-8f63-4c85-9998-db2022caaa52</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
